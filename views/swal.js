function selectLang() {
    const lang = document.createElement('div');
    lang.innerHTML = "<a href='/accueil' class='lang'>Français</a><a href='/home' class='lang'>English</a>";

    swal({content: lang, buttons: false,className: "lang_swal",})
}

function dees() {
    const dees = document.createElement('div');
    let deesValue;
    let min = Math.ceil(2);
    let max = Math.floor(12);
    let rdm = Math.floor(Math.random() * (max - min +1)) + min;
    deesValue = rdm;
    dees.innerHTML = `<p style="font-size: 3vh;">Dees = ${deesValue} !</p><form action="/csnpl_dees" method="POST"><input type="hidden" name="dees" value="${deesValue}" /><input type="submit" style="font-size: 3vh;" value="ok" /></form>`;
    swal({content: dees, buttons: false});
}

function chance() {
    const chance = document.createElement('div');
    let cardList;
    let min;
    let max;
    let rdm;
    let minA = Math.ceil(0);;
    let maxA = Math.floor(1010);
    let rdmA = Math.floor(Math.random() * (maxA - minA +1)) + minA;
    let amount = rdmA;
    let pickedCard;
    if (amount > 1000) {
        cardList = ['Air', 'Terre', 'Tornade', 'Paradis', 'Enfer', 'Cimetière', 'Gain', 'Perte', 'Double', 'Moitier'];
        min = Math.ceil(0);
        max = Math.floor(9);
        rdm = Math.floor(Math.random() * (max - min +1)) + min;
        pickedCard = cardList[rdm];
    } else if (amount > 500) {
        cardList = ['Air', 'Terre', 'Tornade', 'Paradis', 'Enfer', 'Cimetière', 'Gain', 'Perte'];
        min = Math.ceil(0);
        max = Math.floor(7);
        rdm = Math.floor(Math.random() * (max - min +1)) + min;
        pickedCard = cardList[rdm];
    } else if (amount > 250) {
        cardList = ['Air', 'Terre', 'Tornade', 'Paradis', 'Gain', 'Perte'];
        min = Math.ceil(0);
        max = Math.floor(5);
        rdm = Math.floor(Math.random() * (max - min +1)) + min;
        pickedCard = cardList[rdm];
    } else {
        cardList = ['Air', 'Terre', 'Paradis', 'Gain'];
        min = Math.ceil(0);
        max = Math.floor(3);
        rdm = Math.floor(Math.random() * (max - min +1)) + min;
        pickedCard = cardList[rdm];
    };
    console.log(amount,pickedCard);
    let cardEffect;
    if (pickedCard == 'Air') {
        cardEffect = "Sa plus grosse perte est remise à zéro";
    } else if (pickedCard == 'Terre') {
        cardEffect = "Son plus gros gain est remise à zéro !";
    } else if (pickedCard == 'Tornade') {
        cardEffect = "Son adversaire gagne son plus gros gain !";
    } else if (pickedCard == 'Paradis') {
        cardEffect = "Son adversaire reçois sa plus grosse perte !";
    } else if (pickedCard == 'Gain') {
        cardEffect = "Gagne le score de son adversaire !";
    } else if (pickedCard == 'Perte') {
        cardEffect = "Perd l'équivalent du plus gros gain de son adversaire !";
    } else if (pickedCard == 'Enfer') {
        cardEffect = "Les plus gros gain son remis à zéro et doit payer un sub à un follower";
    } else if (pickedCard == 'Cimetière') {
        cardEffect = "Perd la somme des plus grosse perte !";
    } else if (pickedCard == 'Double') {
        cardEffect = "Son plus gros gain est doublé mais doit donner des coins à un sub";
    } else if (pickedCard == 'Moitier') {
        cardEffect = "Son plus gros gain est divisé par 2 et doit donner des coins à un follower";
    } else {
        return;
    }
    chance.innerHTML = `<p style="font-size: 4vh;">Carte : ${pickedCard}</p><br><p style="font-size: 4vh;">${cardEffect}</p><form action="/csnpl_chance" method="POST"><input type="hidden" name="card" value="${pickedCard}" /><input type="submit" style="font-size: 3vh;" value="ok" /></form>`;
    swal({content: chance, buttons: false});
}