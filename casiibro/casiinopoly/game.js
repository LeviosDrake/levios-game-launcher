// IMPORTS
const db = require('./database.js');

// FUNCTIONS
function dbInit() {
    db.init();
};

function dbAddPlayers(j1, j2) {
    db.addPlayers(j1, j2);
};

function dbGenerateBoard() {
    db.generateBoard();
};

function dbGetDatas() {
    return new Promise ((resolve, reject) => {
        db.getDatas()
        .then((datas) => {
            resolve(datas)
        })
    })
};

function dbThrowDices() {
    return new Promise ((resolve, reject) => {
        let min = Math.ceil(2);
        let max = Math.floor(12);
        let rdm = Math.floor(Math.random() * (max - min +1)) + min;
        db.getDatas()
        .then((datas) => {
            datas.game_msg = `${datas[`${datas.game_turn}_nickname`]} fait ${rdm} !`;
            db.updateDatas(datas);
            resolve(rdm);  
        })
    })
};

function dbNextTurn() {
    return new Promise ((resolve, reject) => {
        db.nextTurn()
        .then((pugDatas) => {
            resolve(pugDatas)
        })
    })
};

function dbMovePlayer(amount) {
    return new Promise ((resolve, reject) => {
        db.movePlayer(amount)
        .then((datas) => {
            resolve(datas);
        })
    })
};

function dbAddScore(amount) {
    return new Promise ((resolve, reject) => {
        db.addScore(amount)
        .then((datas) => {
            resolve(datas);
        })
    })
};

function dbPickCard() {
    return new Promise ((resolve, reject) => {
        db.pickCard()
        .then((pugDatas) => {
            resolve(pugDatas)
        })
    })
};

function dbChangeCase() {
    return new Promise ((resolve, reject) => {
        db.changeCase()
        .then((datas) => {
            resolve(datas);
        })
    })
};

function ThrowDices() {
    let min = Math.ceil(2);
    let max = Math.floor(12);
    let rdm = Math.floor(Math.random() * (max - min +1)) + min;
    return rdm;
};

// EXPORTS
module.exports = {
    dbInit,
    dbAddPlayers,
    dbGenerateBoard,
    dbGetDatas,
    dbThrowDices,
    dbNextTurn,
    dbMovePlayer,
    dbAddScore,
    dbPickCard,
    dbChangeCase,
    ThrowDices
}