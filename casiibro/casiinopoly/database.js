// IMPORTS
const fs = require('fs');
const path = require('path');
const url = require('url');
const {app} = require('electron');

// DBPATH
const dbPath = app.getPath('appData') + '/storage.json';

// FUNCTIONS
function init() {
    let datas ={
        j1_nickname: '',
        j1_score: 0,
        j1_higherGain: 0,
        j1_higherLoss: 0,
        j1_case: 1,
        j2_nickname: '',
        j2_score: 0,
        j2_higherGain: 0,
        j2_higherLoss: 0,
        j2_case: 1,
        game_turn: '',
        game_msg: '',
        case_1_name: '',
        case_1_img: '',
        case_1_j1: '',
        case_1_j2: '',
        case_2_name: '',
        case_2_img: '',
        case_2_j1: '',
        case_2_j2: '',
        case_3_name: '',
        case_3_img: '',
        case_3_j1: '',
        case_3_j2: '',
        case_4_name: '',
        case_4_img: '',
        case_4_j1: '',
        case_4_j2: '',
        case_5_name: '',
        case_5_img: '',
        case_5_j1: '',
        case_5_j2: '',
        case_6_name: '',
        case_6_img: '',
        case_6_j1: '',
        case_6_j2: '',
        case_7_name: '',
        case_7_img: '',
        case_7_j1: '',
        case_7_j2: '',
        case_8_name: '',
        case_8_img: '',
        case_8_j1: '',
        case_8_j2: '',
        case_9_name: '',
        case_9_img: '',
        case_9_j1: '',
        case_9_j2: '',
        case_10_name: '',
        case_10_img: '',
        case_10_j1: '',
        case_10_j2: '',
        case_11_name: '',
        case_11_img: '',
        case_11_j1: '',
        case_11_j2: '',
        case_12_name: '',
        case_12_img: '',
        case_12_j1: '',
        case_12_j2: '',
        case_13_name: '',
        case_13_img: '',
        case_13_j1: '',
        case_13_j2: '',
        case_14_name: '',
        case_14_img: '',
        case_14_j1: '',
        case_14_j2: '',
        case_15_name: '',
        case_15_img: '',
        case_15_j1: '',
        case_15_j2: '',
        case_16_name: '',
        case_16_img: '',
        case_16_j1: '',
        case_16_j2: '',
        case_17_name: '',
        case_17_img: '',
        case_17_j1: '',
        case_17_j2: '',
        case_18_name: '',
        case_18_img: '',
        case_18_j1: '',
        case_18_j2: '',
        case_19_name: '',
        case_19_img: '',
        case_19_j1: '',
        case_19_j2: '',
        case_20_name: '',
        case_20_img: '',
        case_20_j1: '',
        case_20_j2: '',
        case_21_name: '',
        case_21_img: '',
        case_21_j1: '',
        case_21_j2: '',
        case_22_name: '',
        case_22_img: '',
        case_22_j1: '',
        case_22_j2: '',
        case_23_name: '',
        case_23_img: '',
        case_23_j1: '',
        case_23_j2: '',
        case_24_name: '',
        case_24_img: '',
        case_24_j1: '',
        case_24_j2: '',
        case_25_name: '',
        case_25_img: '',
        case_25_j1: '',
        case_25_j2: '',
        case_26_name: '',
        case_26_img: '',
        case_26_j1: '',
        case_26_j2: '',
        case_27_name: '',
        case_27_img: '',
        case_27_j1: '',
        case_27_j2: '',
        case_28_name: '',
        case_28_img: '',
        case_28_j1: '',
        case_28_j2: '',
        case_29_name: '',
        case_29_img: '',
        case_29_j1: '',
        case_29_j2: '',
        case_30_name: '',
        case_30_img: '',
        case_30_j1: '',
        case_30_j2: '',
        case_31_name: '',
        case_31_img: '',
        case_31_j1: '',
        case_31_j2: '',
        case_32_name: '',
        case_32_img: '',
        case_32_j1: '',
        case_32_j2: '',
        case_33_name: '',
        case_33_img: '',
        case_33_j1: '',
        case_33_j2: '',
        case_34_name: '',
        case_34_img: '',
        case_34_j1: '',
        case_34_j2: '',
        case_35_name: '',
        case_35_img: '',
        case_35_j1: '',
        case_35_j2: '',
        case_36_name: '',
        case_36_img: '',
        case_36_j1: '',
        case_36_j2: '',
        case_37_name: '',
        case_37_img: '',
        case_37_j1: '',
        case_37_j2: '',
        case_38_name: '',
        case_38_img: '',
        case_38_j1: '',
        case_38_j2: '',
        case_39_name: '',
        case_39_img: '',
        case_39_j1: '',
        case_39_j2: '',
        case_40_name: '',
        case_40_img: '',
        case_40_j1: '',
        case_40_j2: '',
    };
    let min = Math.ceil(1);
    let max = Math.floor(2);
    let rdm = Math.floor(Math.random() * (max - min +1)) + min;
    datas.game_turn = `j${rdm}`;
    let jsonObj = JSON.stringify(datas, undefined, 2);
    fs.writeFile(dbPath, jsonObj, (err) => {
        if (err) throw err;
    })
};

function addPlayers(j1, j2) {
    fs.readFile(dbPath, (err, data) => {
        if (err) throw err;
        let datas = JSON.parse(data);
        datas.j1_nickname = j1;
        datas.j2_nickname = j2;
        datas.game_msg = `C'est à ${datas[`${datas.game_turn}_nickname`]} de commencer !`;
        let jsonObj = JSON.stringify(datas);
        fs.writeFile(dbPath, jsonObj, (err) => {
            if (err) throw err;
        })
    })
};

function generateBoard() {
    fs.readFile(dbPath, (err, data) => {
        let datas = JSON.parse(data);
        let casesList = {
            case_1 : {
                name : '300 Shields',
                path : '../../public/cases_img/300Shields.png'
            },
            case_2 : {
                name : 'Bonanza',
                path : '../../public/cases_img/Bonanza.png'
            },
            case_3 : {
                name : 'Book Of Aztec',
                path : '../../public/cases_img/BookOfAztec.png'
            },
            case_4 : {
                name : 'Book of crazy chicken',
                path : '../../public/cases_img/Bookofcrazychicken.png'
            },
            case_5 : {
                name : 'Book of Dead',
                path : '../../public/cases_img/BookofDead.png'
            },
            case_6 : {
                name : 'Book of Lords',
                path : '../../public/cases_img/BookofLords.png'
            },
            case_7 : {
                name : 'Book of queen',
                path : '../../public/cases_img/Bookofqueen.png'
            },
            case_8 : {
                name : 'Book&bulls',
                path : '../../public/cases_img/Book&bulls.png'
            },
            case_9 : {
                name : 'Carnival Queen',
                path : '../../public/cases_img/CarnivalQueen.png'
            },
            case_10 : {
                name : 'Cazino Cosmos',
                path : '../../public/cases_img/CazinoCosmos.png'
            },
            case_11 : {
                name : 'Danger High Voltage',
                path : '../../public/cases_img/DangerHighVoltage.png'
            },
            case_12 : {
                name : 'Dead or Alive',
                path : '../../public/cases_img/DeadorAlive.png'
            },
            case_13 : {
                name : 'DragonHorn',
                path : '../../public/cases_img/DragonHorn.png'
            },
            case_14 : {
                name : 'Extra Chilli',
                path : '../../public/cases_img/ExtraChilli.png'
            },
            case_15 : {
                name : 'Extra Juicy',
                path : '../../public/cases_img/ExtraJuicy.png'
            },
            case_16 : {
                name : 'Eyes of horus',
                path : '../../public/cases_img/Eyesofhorus.png'
            },
            case_17 : {
                name : 'Firefly Frenzy',
                path : '../../public/cases_img/FireflyFrenzy.png'
            },
            case_18 : {
                name : 'Fish Golden Tank',
                path : '../../public/cases_img/FishGoldenTank.png'
            },
            case_19 : {
                name : 'Fish Party',
                path : '../../public/cases_img/FishParty.png'
            },
            case_20 : {
                name : 'Fishin Freenzy',
                path : '../../public/cases_img/FishinFreenzy.png'
            },
            case_21 : {
                name : 'Flame Busters',
                path : '../../public/cases_img/FlameBusters.png'
            },
            case_22 : {
                name : 'Jack And the Beanstalk',
                path : '../../public/cases_img/JackAndtheBeanstalk.png'
            },
            case_23 : {
                name : 'John Hunter and the Aztec Treasure',
                path : '../../public/cases_img/JohnHunterandtheAztecTreasure.png'
            },
            case_24 : {
                name : 'John hunter and the Secrets Of Da Vincis Treasure',
                path : '../../public/cases_img/JohnhunterandtheSecretsOfDaVincisTreasure.png'
            },
            case_25 : {
                name : 'Jungle Spirit',
                path : '../../public/cases_img/JungleSpirit.png'
            },
            case_26 : {
                name : 'King of the jungle',
                path : '../../public/cases_img/Kingofthejungle.png'
            },
            case_27 : {
                name : 'Knights life',
                path : '../../public/cases_img/Knightslife.png'
            },
            case_28 : {
                name : 'Legacy Of Eghypt',
                path : '../../public/cases_img/LegacyOfEghypt.png'
            },
            case_29 : {
                name : 'Lost Relics',
                path : '../../public/cases_img/LostRelics.png'
            },
            case_30 : {
                name : 'Madame Destiny',
                path : '../../public/cases_img/MadameDestiny.png'
            },
            case_31 : {
                name : 'Magic Mirror',
                path : '../../public/cases_img/MagicMirror.png'
            },
            case_32 : {
                name : 'Magic Stone',
                path : '../../public/cases_img/MagicStone.png'
            },
            case_33 : {
                name : 'Millionaire Slot',
                path : '../../public/cases_img/MillionaireSlot.png'
            },
            case_34 : {
                name : 'moon',
                path : '../../public/cases_img/moon.png'
            },
            case_35 : {
                name : 'Mustang Gold',
                path : '../../public/cases_img/MustangGold.png'
            },
            case_36 : {
                name : 'Peking Luck',
                path : '../../public/cases_img/PekingLuck.png'
            },
            case_37 : {
                name : 'Pink Elephants',
                path : '../../public/cases_img/PinkElephants.png'
            },
            case_38 : {
                name : 'Pirate Gold',
                path : '../../public/cases_img/PirateGold.png'
            },
            case_39 : {
                name : 'Raging Rex',
                path : '../../public/cases_img/RagingRex.png'
            },
            case_40 : {
                name : 'Ramses Book',
                path : '../../public/cases_img/RamsesBook.png'
            },
            case_41 : {
                name : 'Rise Of Dead',
                path : '../../public/cases_img/RiseOfDead.png'
            },
            case_42 : {
                name : 'Rise of Merlin',
                path : '../../public/cases_img/RiseofMerlin.png'
            },
            case_43 : {
                name : 'Rise Of Olympius',
                path : '../../public/cases_img/RiseOfOlympius.png'
            },
            case_44 : {
                name : 'Starburst',
                path : '../../public/cases_img/Starburst.png'
            },
            case_45 : {
                name : 'Street',
                path : '../../public/cases_img/Street.png'
            },
            case_46 : {
                name : 'Sweet Bonanza',
                path : '../../public/cases_img/SweetBonanza.png'
            },
            case_47 : {
                name : 'The Dog House',
                path : '../../public/cases_img/TheDogHouse.png'
            },
            case_48 : {
                name : 'The Falcon Huntress',
                path : '../../public/cases_img/TheFalconHuntress.png'
            },
            case_49 : {
                name : 'The Sword & the Grail',
                path : '../../public/cases_img/TheSword&theGrail.png'
            },
            case_50 : {
                name : 'ThunderTruck',
                path : '../../public/cases_img/ThunderTruck.png'
            },
            case_51 : {
                name : 'Valley of the gods',
                path : '../../public/cases_img/Valleyofthegods.png'
            },
            case_52 : {
                name : 'Vikings',
                path : '../../public/cases_img/Vikings.png'
            },
            case_53 : {
                name : 'White Rabbit',
                path : '../../public/cases_img/WhiteRabbit.png'
            },
            case_54 : {
                name : 'Wolf Gold',
                path : '../../public/cases_img/WolfGold.png'
            },
            case_55 : {
                name : 'Yak Yeti and Roll',
                path : '../../public/cases_img/YakYetiandRoll.png'
            }
        };
        for (let i = 1 ; i <= 40 ; i ++) {
            let min = Math.ceil(1);
            let max = Math.floor(55);
            let rdm = Math.floor(Math.random() * (max - min +1)) + min;
            if (i == 1) {
                datas.case_1_name = 'Start';
                datas.case_1_img = '../../public/cases_img/case_start.png';
                datas.case_1_j1 = '../../public/img/Pionmauve.png'; 
                datas.case_1_j2 = '../../public/img/Pionorange.png';
            } else if (i == 11 || i == 31) {
                datas[`case_${i}_name`] = 'Prison';
                datas[`case_${i}_img`] = '../../public/cases_img/case_prison.png';
            } else if (i == 5 || i == 12 || i == 15 || i == 20 || i == 27 || i == 32 || i == 35 || i == 38) {
                datas[`case_${i}_name`] = 'Chance';
                datas[`case_${i}_img`] = '../../public/cases_img/case_chance.png';
            } else {
                datas[`case_${i}_name`] = casesList[`case_${rdm}`].name;
                datas[`case_${i}_img`] = casesList[`case_${rdm}`].path;
            }
        }
        let jsonObj = JSON.stringify(datas);
        fs.writeFile(dbPath, jsonObj, (err) => {
            if (err) throw err;
        })
    })
};

function getDatas() {
    return new Promise ((resolve, reject) => {
        fs.readFile(dbPath, (err, data) => {
            if (err) throw err;
            resolve(JSON.parse(data));
        })
    })
};

function updateDatas(datas) {
    let jsonObj = JSON.stringify(datas);
    fs.writeFile(dbPath, jsonObj, (err) => {
        if (err) throw err;
    })
}

function movePlayer(amount) {
    return new Promise ((resolve, reject) => {
        getDatas()
        .then((datas) => {
            let playerId = datas.game_turn;
            let currentCase = datas[`${playerId}_case`];
            let caseAfterMove = currentCase + amount;
            let playerPion = datas[`case_${currentCase}_${playerId}`];
            datas[`case_${currentCase}_${playerId}`] = '';
            datas[`case_${caseAfterMove}_${playerId}`] = playerPion;
            let newCase = datas[`case_${caseAfterMove}_name`];
            if (newCase == 'Start') {
                datas.game_msg = datas.game_msg + `${datas[`${playerId}_nickname`]} est à la case départ !`;
            } else if (newCase == 'Chance') {
                datas.game_msg = datas.game_msg + `${datas[`${playerId}_nickname`]} piochez une carte chance !`;
            } else if (newCase == 'Prison') {
                datas.game_msg = datas.game_msg + `${datas[`${playerId}_nickname`]} est en prison !`;
            } else {
                datas.game_msg = datas.game_msg + `${datas[`${playerId}_nickname`]} doit jouer à ${datas[`case_${caseAfterMove}_name`]}`;
            }
            datas[`${playerId}_case`] = caseAfterMove;
            let jsonObj = JSON.stringify(datas);
            fs.writeFile(dbPath, jsonObj, (err) => {
                if (err) throw err;
                resolve(datas)
            })
        })
    })
};

function nextTurn() {
    return new Promise ((resolve, reject) => {
        fs.readFile(dbPath, (err, data) => {
            let datas;
            datas = JSON.parse(data);
            if (datas.game_turn == 'j1') {
                datas.game_turn = 'j2';
                datas.game_msg = datas.game_msg + `A ${datas[`${datas.game_turn}_nickname`]} de jouer`;
            } else {
                datas.game_turn = 'j1';
                datas.game_msg = datas.game_msg + `A ${datas[`${datas.game_turn}_nickname`]} de jouer`;
            }
            let jsonObj = JSON.stringify(datas);
            fs.writeFile(dbPath, jsonObj, (err) => {
                if (err) throw err;
                resolve(datas)
            })
        })
    })
};

function addScore(amount) {
    return new Promise ((resolve, reject) => {
        fs.readFile(dbPath, (err, data) => {
            let datas;
            datas = JSON.parse(data);
            let currentPlayer = datas.game_turn;
            let playerHigherGain = datas[`${currentPlayer}_higherGain`];
            let playerHigherLoss = datas[`${currentPlayer}_higherLoss`];
            if (amount > playerHigherGain) {
                datas[`${currentPlayer}_higherGain`] = amount;
                datas[`${currentPlayer}_score`] = datas[`${currentPlayer}_score`] + amount;
            } else if (amount < playerHigherLoss) {
                datas[`${currentPlayer}_higherLoss`] = amount;
                datas[`${currentPlayer}_score`] = datas[`${currentPlayer}_score`] + amount;
            } else {
                datas[`${currentPlayer}_score`] = datas[`${currentPlayer}_score`] + amount;
            };
            if (datas.game_turn == 'j1') {
                datas.game_turn = 'j2';
                datas.game_msg = `A ${datas[`${datas.game_turn}_nickname`]} de jouer !`;
            } else {
                datas.game_turn = 'j1';
                datas.game_msg = `A ${datas[`${datas.game_turn}_nickname`]} de jouer !`;
            };
            let jsonObj = JSON.stringify(datas);
            fs.writeFile(dbPath, jsonObj, (err) => {
                if (err) throw err;
                resolve(datas);
            })
        })
    })
};

function pickCard() {
    return new Promise ((resolve, reject) => {
        fs.readFile(dbPath, (err, data) => {
            let datas;
            datas = JSON.parse(data);
            let cardList;
            let gainAmount = datas.j1_higherGain + datas.j2_higherGain;
            let min;
            let max;
            let rdm;
            let pickedCard;
            if (gainAmount > 1000) {
                cardList = ['Air', 'Terre', 'Tornade', 'Paradis', 'Enfer', 'Cimetière', 'Gain', 'Perte', 'Double', 'Moitier'];
                min = Math.ceil(0);
                max = Math.floor(9);
                rdm = Math.floor(Math.random() * (max - min +1)) + min;
                pickedCard = cardList[rdm];
                cardEffect(pickedCard)
                .then((datas) => {
                    resolve(datas);
                })
            } else if (gainAmount > 500) {
                cardList = ['Air', 'Terre', 'Tornade', 'Paradis', 'Enfer', 'Cimetière', 'Gain', 'Perte'];
                min = Math.ceil(0);
                max = Math.floor(7);
                rdm = Math.floor(Math.random() * (max - min +1)) + min;
                pickedCard = cardList[rdm];
                cardEffect(pickedCard)
                .then((datas) => {
                    resolve(datas);
                })
            } else if (gainAmount > 250) {
                cardList = ['Air', 'Terre', 'Tornade', 'Paradis', 'Gain', 'Perte'];
                min = Math.ceil(0);
                max = Math.floor(5);
                rdm = Math.floor(Math.random() * (max - min +1)) + min;
                pickedCard = cardList[rdm];
                cardEffect(pickedCard)
                .then((datas) => {
                    resolve(datas);
                })
            } else {
                cardList = ['Air', 'Terre', 'Paradis', 'Gain'];
                min = Math.ceil(0);
                max = Math.floor(3);
                rdm = Math.floor(Math.random() * (max - min +1)) + min;
                pickedCard = cardList[rdm];
                cardEffect(pickedCard)
                .then((datas) => {
                    resolve(datas);
                })
            };


        })
    })
};

function cardEffect(card) {
    return new Promise ((resolve, reject) => {
        fs.readFile(dbPath, (err, data) => {
            let datas;
            datas = JSON.parse(data);
            let playerId = datas.game_turn;
            if (card == 'Air') {
                datas.game_msg = `${datas[`${playerId}_nickname`]} a piocher la carte ${card}. Sa plus grosse perte est remise à zéro`;
                datas[`${playerId}_score`] = datas[`${playerId}_score`] - datas[`${playerId}_higherLoss`];
                datas[`${playerId}_higherLoss`] = 0;
                if (playerId == 'j1') {
                    datas.game_turn = 'j2';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                } else {
                    datas.game_turn = 'j1';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                }
            } else if (card == 'Terre') {
                datas.game_msg = `${datas[`${playerId}_nickname`]} a piocher la carte ${card}. Son plus gros gain est remise à zéro !`;
                datas[`${playerId}_score`] = datas[`${playerId}_score`] - datas[`${playerId}_higherGain`];
                datas[`${playerId}_higherGain`] = 0;
                if (playerId == 'j1') {
                    datas.game_turn = 'j2';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                } else {
                    datas.game_turn = 'j1';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                }
            } else if (card == 'Tornade') {
                datas.game_msg = `${datas[`${playerId}_nickname`]} a piocher la carte ${card}. Son adversaire gagne son plus gros gain !`;
                if (playerId == 'j1') {
                    datas.j2_score = datas.j2_score + datas.j1_higherGain;
                    datas.game_turn = 'j2';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                } else {
                    datas.j1_score = datas.j1_score + datas.j2_higherGain;
                    datas.game_turn = 'j1';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                }
            } else if (card == 'Paradis') {
                datas.game_msg = `${datas[`${playerId}_nickname`]} a piocher la carte ${card}. Son adversaire reçois sa plus grosse perte !`;
                if (playerId == 'j1') {
                    datas.j2_score = datas.j2_score + datas.j1_higherLoss;
                    datas.game_turn = 'j2';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                } else {
                    datas.j1_score = datas.j1_score + datas.j2_higherLoss;
                    datas.game_turn = 'j1';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                }
            } else if (card == 'Gain') {
                datas.game_msg = `${datas[`${playerId}_nickname`]} a piocher la carte ${card}. Il gagne le score de son adversaire !`;
                if (playerId == 'j1') {
                    datas.j1_score = datas.j1_score + datas.j2_score;
                    datas.game_turn = 'j2';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                } else {
                    datas.j2_score = datas.j2_score + datas.j1_score;
                    datas.game_turn = 'j1';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                }
            } else if (card == 'Perte') {
                datas.game_msg = `${datas[`${playerId}_nickname`]} a piocher la carte ${card}. Il perd l'équivalent du plus gros gain de son adversaire !`;
                if (playerId == 'j1') {
                    datas.j1_score = datas.j1_score - datas.j2_higherGain;
                    datas.game_turn = 'j2';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                } else {
                    datas.j2_score = datas.j2_score - datas.j1_higherGain;
                    datas.game_turn = 'j1';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                }
            } else if (card == 'Enfer') {
                datas.game_msg = `${datas[`${playerId}_nickname`]} a piocher la carte ${card}. Les plus gros gain son remis à zéro et doit payer un sub à un follower`;
                datas.j1_score = datas.j1_score - datas.j1_higherGain;
                datas.j1_higherGain = 0;
                datas.j2_score = datas.j2_score - datas.j2_higherGain;
                datas.j2_higherGain = 0;
                if (playerId == 'j1') {
                    datas.game_turn = 'j2';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                } else {
                    datas.game_turn = 'j1';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                }
            } else if (card == 'Cimetière') {
                datas.game_msg = `${datas[`${playerId}_nickname`]} a piocher la carte ${card}. Il perd la somme des plus grosse perte !`;
                let sumLoss = datas.j1_higherLoss + datas.j2_higherLoss;
                if (playerId == 'j1') {
                    datas.j1_score = datas.j1_score - sumLoss;
                    datas.game_turn = 'j2';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                } else {
                    datas.j2_score = datas.j2_score - sumLoss;
                    datas.game_turn = 'j1';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                }
            } else if (card == 'Double') {
                datas.game_msg = `${datas[`${playerId}_nickname`]} a piocher la carte ${card}. Son plus gros gain est doublé mais doit donner des coins à un sub`;
                if (playerId == 'j1') {
                    datas.j1_score = datas.j1_score + datas.j1_higherGain;
                    datas.j1_higherGain = datas.j1_higherGain + datas.j1_higherGain;
                    datas.game_turn = 'j2';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                } else {
                    datas.j2_score = datas.j2_score + datas.j2_higherGain;
                    datas.j2_higherGain = datas.j2_higherGain + datas.j2_higherGain;
                    datas.game_turn = 'j1';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                }
            } else if (card == 'Moitier') {
                datas.game_msg = `${datas[`${playerId}_nickname`]} a piocher la carte ${card}. Son plus gros gain est divisé par 2 et doit donner des coins à un follower`;
                if (playerId == 'j1') {
                    datas.j1_higherGain = datas.j1_higherGain / 2;
                    datas.j1_score = datas.j1_score - datas.j1_higherGain;
                    datas.game_turn = 'j2';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                } else {
                    datas.j2_higherGain = datas.j2_higherGain / 2;
                    datas.j2_score = datas.j2_score - datas.j2_higherGain;
                    datas.game_turn = 'j1';
                    datas.game_msg = datas.game_msg + ' ' + `${datas[`${datas.game_turn}_nickname`]} a toi de jouer !`;
                    let jsonObj = JSON.stringify(datas);
                    fs.writeFile(dbPath, jsonObj, (err) => {
                        if (err) throw err;
                        resolve(datas);
                    })
                }
            } else {
                console.error('Card missmatch');
            }
        })
    })
};

function changeCase() {
    return new Promise ((resolve, reject) => {
        fs.readFile(dbPath, (err, data) => {
            let datas;
            datas = JSON.parse(data);
            let playerId = datas.game_turn;
            let playerPion = datas[`case_${datas[`${playerId}_case`]}_${playerId}`];
            let newCaseName;
            let newCaseImg;
            let casesList = {
                case_1 : {
                    name : '300 Shields',
                    path : '../../public/cases_img/300Shields.png'
                },
                case_2 : {
                    name : 'Bonanza',
                    path : '../../public/cases_img/Bonanza.png'
                },
                case_3 : {
                    name : 'Book Of Aztec',
                    path : '../../public/cases_img/BookOfAztec.png'
                },
                case_4 : {
                    name : 'Book of crazy chicken',
                    path : '../../public/cases_img/Bookofcrazychicken.png'
                },
                case_5 : {
                    name : 'Book of Dead',
                    path : '../../public/cases_img/BookofDead.png'
                },
                case_6 : {
                    name : 'Book of Lords',
                    path : '../../public/cases_img/BookofLords.png'
                },
                case_7 : {
                    name : 'Book of queen',
                    path : '../../public/cases_img/Bookofqueen.png'
                },
                case_8 : {
                    name : 'Book&bulls',
                    path : '../../public/cases_img/Book&bulls.png'
                },
                case_9 : {
                    name : 'Carnival Queen',
                    path : '../../public/cases_img/CarnivalQueen.png'
                },
                case_10 : {
                    name : 'Cazino Cosmos',
                    path : '../../public/cases_img/CazinoCosmos.png'
                },
                case_11 : {
                    name : 'Danger High Voltage',
                    path : '../../public/cases_img/DangerHighVoltage.png'
                },
                case_12 : {
                    name : 'Dead or Alive',
                    path : '../../public/cases_img/DeadorAlive.png'
                },
                case_13 : {
                    name : 'DragonHorn',
                    path : '../../public/cases_img/DragonHorn.png'
                },
                case_14 : {
                    name : 'Extra Chilli',
                    path : '../../public/cases_img/ExtraChilli.png'
                },
                case_15 : {
                    name : 'Extra Juicy',
                    path : '../../public/cases_img/ExtraJuicy.png'
                },
                case_16 : {
                    name : 'Eyes of horus',
                    path : '../../public/cases_img/Eyesofhorus.png'
                },
                case_17 : {
                    name : 'Firefly Frenzy',
                    path : '../../public/cases_img/FireflyFrenzy.png'
                },
                case_18 : {
                    name : 'Fish Golden Tank',
                    path : '../../public/cases_img/FishGoldenTank.png'
                },
                case_19 : {
                    name : 'Fish Party',
                    path : '../../public/cases_img/FishParty.png'
                },
                case_20 : {
                    name : 'Fishin Freenzy',
                    path : '../../public/cases_img/FishinFreenzy.png'
                },
                case_21 : {
                    name : 'Flame Busters',
                    path : '../../public/cases_img/FlameBusters.png'
                },
                case_22 : {
                    name : 'Jack And the Beanstalk',
                    path : '../../public/cases_img/JackAndtheBeanstalk.png'
                },
                case_23 : {
                    name : 'John Hunter and the Aztec Treasure',
                    path : '../../public/cases_img/JohnHunterandtheAztecTreasure.png'
                },
                case_24 : {
                    name : 'John hunter and the Secrets Of Da Vincis Treasure',
                    path : '../../public/cases_img/JohnhunterandtheSecretsOfDaVincisTreasure.png'
                },
                case_25 : {
                    name : 'Jungle Spirit',
                    path : '../../public/cases_img/JungleSpirit.png'
                },
                case_26 : {
                    name : 'King of the jungle',
                    path : '../../public/cases_img/Kingofthejungle.png'
                },
                case_27 : {
                    name : 'Knights life',
                    path : '../../public/cases_img/Knightslife.png'
                },
                case_28 : {
                    name : 'Legacy Of Eghypt',
                    path : '../../public/cases_img/LegacyOfEghypt.png'
                },
                case_29 : {
                    name : 'Lost Relics',
                    path : '../../public/cases_img/LostRelics.png'
                },
                case_30 : {
                    name : 'Madame Destiny',
                    path : '../../public/cases_img/MadameDestiny.png'
                },
                case_31 : {
                    name : 'Magic Mirror',
                    path : '../../public/cases_img/MagicMirror.png'
                },
                case_32 : {
                    name : 'Magic Stone',
                    path : '../../public/cases_img/MagicStone.png'
                },
                case_33 : {
                    name : 'Millionaire Slot',
                    path : '../../public/cases_img/MillionaireSlot.png'
                },
                case_34 : {
                    name : 'moon',
                    path : '../../public/cases_img/moon.png'
                },
                case_35 : {
                    name : 'Mustang Gold',
                    path : '../../public/cases_img/MustangGold.png'
                },
                case_36 : {
                    name : 'Peking Luck',
                    path : '../../public/cases_img/PekingLuck.png'
                },
                case_37 : {
                    name : 'Pink Elephants',
                    path : '../../public/cases_img/PinkElephants.png'
                },
                case_38 : {
                    name : 'Pirate Gold',
                    path : '../../public/cases_img/PirateGold.png'
                },
                case_39 : {
                    name : 'Raging Rex',
                    path : '../../public/cases_img/RagingRex.png'
                },
                case_40 : {
                    name : 'Ramses Book',
                    path : '../../public/cases_img/RamsesBook.png'
                },
                case_41 : {
                    name : 'Rise Of Dead',
                    path : '../../public/cases_img/RiseOfDead.png'
                },
                case_42 : {
                    name : 'Rise of Merlin',
                    path : '../../public/cases_img/RiseofMerlin.png'
                },
                case_43 : {
                    name : 'Rise Of Olympius',
                    path : '../../public/cases_img/RiseOfOlympius.png'
                },
                case_44 : {
                    name : 'Starburst',
                    path : '../../public/cases_img/Starburst.png'
                },
                case_45 : {
                    name : 'Street',
                    path : '../../public/cases_img/Street.png'
                },
                case_46 : {
                    name : 'Sweet Bonanza',
                    path : '../../public/cases_img/SweetBonanza.png'
                },
                case_47 : {
                    name : 'The Dog House',
                    path : '../../public/cases_img/TheDogHouse.png'
                },
                case_48 : {
                    name : 'The Falcon Huntress',
                    path : '../../public/cases_img/TheFalconHuntress.png'
                },
                case_49 : {
                    name : 'The Sword & the Grail',
                    path : '../../public/cases_img/TheSword&theGrail.png'
                },
                case_50 : {
                    name : 'ThunderTruck',
                    path : '../../public/cases_img/ThunderTruck.png'
                },
                case_51 : {
                    name : 'Valley of the gods',
                    path : '../../public/cases_img/Valleyofthegods.png'
                },
                case_52 : {
                    name : 'Vikings',
                    path : '../../public/cases_img/Vikings.png'
                },
                case_53 : {
                    name : 'White Rabbit',
                    path : '../../public/cases_img/WhiteRabbit.png'
                },
                case_54 : {
                    name : 'Wolf Gold',
                    path : '../../public/cases_img/WolfGold.png'
                },
                case_55 : {
                    name : 'Yak Yeti and Roll',
                    path : '../../public/cases_img/YakYetiandRoll.png'
                }
            };
            let min = Math.ceil(1);
            let max = Math.floor(55);
            let rdm = Math.floor(Math.random() * (max - min +1)) + min;
            newCaseName = casesList[`case_${rdm}`].name;
            newCaseImg = casesList[`case_${rdm}`].path;
            datas[`case_${datas[`${datas.game_turn}_case`]}_name`] = newCaseName;
            datas[`case_${datas[`${datas.game_turn}_case`]}_img`] = newCaseImg;
            datas.game_msg = `${datas[`${datas.game_turn}_nickname`]} doit jouer à ${datas[`case_${datas[`${datas.game_turn}_case`]}_name`]}`;
            let jsonObj = JSON.stringify(datas);
            fs.writeFile(dbPath, jsonObj, (err) => {
                if (err) throw err;
                resolve(datas);
            })
        })
    })
};

// EXPORTS
module.exports = {
    init,
    addPlayers,
    generateBoard,
    getDatas,
    movePlayer,
    nextTurn,
    addScore,
    pickCard,
    cardEffect,
    changeCase,
    updateDatas
}