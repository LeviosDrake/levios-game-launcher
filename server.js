// IMPORTS
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const pug = require('pug');

// APP
const app = express();
// => VIEW ENGINE
app.set('views',(path.join(__dirname, '/views')));
app.set('view engine', 'pug');
// => BODY PARSER
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// => PUBLIC FOLDER
app.use('/public', express.static(__dirname + '/public'));

// LISTENER
app.listen(80, () => {
    console.log('Listen on port 80');
});

// LEVIOS
app.get('/', (req, res) => {
    res.render('index');
});

//  FR
app.get('/accueil', (req, res) => {
    res.render('accueil');
});

app.get('/access_fr', (req, res) => {
    res.render('access_fr');
});

app.post('/access_fr', (req, res) => {
    if (req.body.code == "casiibro2019") {
        res.render('casiibro')
    } else {
        res.render('index')
    }
});

// CASIIBRO

// CASIINOPOLY
let casiinopoly;
app.get('/casiinopoly', (req, res) => {
    res.render('csnpl_players')
});

app.post('/csnpl_players', (req, res) => {
    casiinopoly = {
        player1: {
            id: "j1",
            nickname: req.body.j1,
            score: 0,
            higherGain: 0,
            higherLoss: 0,
            case: 1
        },
        player2: {
            id: "j2",
            nickname: req.body.j2,
            score: 0,
            higherGain: 0,
            higherLoss: 0,
            case: 1
        },
        gameboard:{
            case1_name: "",
            case1_img: '',
            case1_pion: '',
            case2_name: "",
            case2_img: '',
            case2_pion: '',
            case3_name: "",
            case3_img: '',
            case3_pion: '',
            case4_name: "",
            case4_img: '',
            case4_pion: '',
            case5_name: "",
            case5_img: '',
            case5_pion: '',
            case6_name: "",
            case6_img: '',
            case6_pion: '',
            case7_name: "",
            case7_img: '',
            case7_pion: '',
            case8_name: "",
            case8_img: '',
            case8_pion: '',
            case9_name: "",
            case9_img: '',
            case9_pion: '',
            case10_name: "",
            case10_img: '',
            case10_pion: '',
            case11_name: "",
            case11_img: '',
            case11_pion: '',
            case12_name: "",
            case12_img: '',
            case12_pion: '',
            case13_name: "",
            case13_img: '',
            case13_pion: '',
            case14_name: "",
            case14_img: '',
            case14_pion: '',
            case15_name: "",
            case15_img: '',
            case15_pion: '',
            case16_name: "",
            case16_img: '',
            case16_pion: '',
            case17_name: "",
            case17_img: '',
            case17_pion: '',
            case18_name: "",
            case18_img: '',
            case18_pion: '',
            case19_name: "",
            case19_img: '',
            case19_pion: '',
            case20_name: "",
            case20_img: '',
            case20_pion: '',
            case21_name: "",
            case21_img: '',
            case21_pion: '',
            case22_name: "",
            case22_img: '',
            case22_pion: '',
            case23_name: "",
            case23_img: '',
            case23_pion: '',
            case24_name: "",
            case24_img: '',
            case24_pion: '',
            case25_name: "",
            case25_img: '',
            case25_pion: '',
            case26_name: "",
            case26_img: '',
            case26_pion: '',
            case27_name: "",
            case27_img: '',
            case27_pion: '',
            case28_name: "",
            case28_img: '',
            case28_pion: '',
            case29_name: "",
            case29_img: '',
            case29_pion: '',
            case30_name: "",
            case30_img: '',
            case30_pion: '',
            case31_name: "",
            case31_img: '',
            case31_pion: '',
            case32_name: "",
            case32_img: '',
            case32_pion: '',
            case33_name: "",
            case33_img: '',
            case33_pion: '',
            case34_name: "",
            case34_img: '',
            case34_pion: '',
            case35_name: "",
            case35_img: '',
            case35_pion: '',
            case36_name: "",
            case36_img: '',
            case36_pion: '',
            case37_name: "",
            case37_img: '',
            case37_pion: '',
            case38_name: "",
            case38_img: '',
            case38_pion: '',
            case39_name: "",
            case39_img: '',
            case39_pion: '',
            case40_name: "",
            case40_img: '',
            case40_pion: '',
        }, 
        infoBar: {
            img: '', 
            msg: ""
        }, 
        turn: ""
    };
    let casesList = {
        case_1 : {
            name : '300 Shields',
            path : './public/cases_img/300Shields.png'
        },
        case_2 : {
            name : 'Bonanza',
            path : './public/cases_img/Bonanza.png'
        },
        case_3 : {
            name : 'Book Of Aztec',
            path : './public/cases_img/BookOfAztec.png'
        },
        case_4 : {
            name : 'Book of crazy chicken',
            path : './public/cases_img/Bookofcrazychicken.png'
        },
        case_5 : {
            name : 'Book of Dead',
            path : './public/cases_img/BookofDead.png'
        },
        case_6 : {
            name : 'Book of Lords',
            path : './public/cases_img/BookofLords.png'
        },
        case_7 : {
            name : 'Book of queen',
            path : './public/cases_img/Bookofqueen.png'
        },
        case_8 : {
            name : 'Book&bulls',
            path : './public/cases_img/Book&bulls.png'
        },
        case_9 : {
            name : 'Carnival Queen',
            path : './public/cases_img/CarnivalQueen.png'
        },
        case_10 : {
            name : 'Cazino Cosmos',
            path : './public/cases_img/CazinoCosmos.png'
        },
        case_11 : {
            name : 'Danger High Voltage',
            path : './public/cases_img/DangerHighVoltage.png'
        },
        case_12 : {
            name : 'Dead or Alive',
            path : './public/cases_img/DeadorAlive.png'
        },
        case_13 : {
            name : 'DragonHorn',
            path : './public/cases_img/DragonHorn.png'
        },
        case_14 : {
            name : 'Extra Chilli',
            path : './public/cases_img/ExtraChilli.png'
        },
        case_15 : {
            name : 'Extra Juicy',
            path : './public/cases_img/ExtraJuicy.png'
        },
        case_16 : {
            name : 'Eyes of horus',
            path : './public/cases_img/Eyesofhorus.png'
        },
        case_17 : {
            name : 'Firefly Frenzy',
            path : './public/cases_img/FireflyFrenzy.png'
        },
        case_18 : {
            name : 'Fish Golden Tank',
            path : './public/cases_img/FishGoldenTank.png'
        },
        case_19 : {
            name : 'Fish Party',
            path : './public/cases_img/FishParty.png'
        },
        case_20 : {
            name : 'Fishin Freenzy',
            path : './public/cases_img/FishinFreenzy.png'
        },
        case_21 : {
            name : 'Flame Busters',
            path : './public/cases_img/FlameBusters.png'
        },
        case_22 : {
            name : 'Jack And the Beanstalk',
            path : './public/cases_img/JackAndtheBeanstalk.png'
        },
        case_23 : {
            name : 'John Hunter and the Aztec Treasure',
            path : './public/cases_img/JohnHunterandtheAztecTreasure.png'
        },
        case_24 : {
            name : 'John hunter and the Secrets Of Da Vincis Treasure',
            path : './public/cases_img/JohnhunterandtheSecretsOfDaVincisTreasure.png'
        },
        case_25 : {
            name : 'Jungle Spirit',
            path : './public/cases_img/JungleSpirit.png'
        },
        case_26 : {
            name : 'King of the jungle',
            path : './public/cases_img/Kingofthejungle.png'
        },
        case_27 : {
            name : 'Knights life',
            path : './public/cases_img/Knightslife.png'
        },
        case_28 : {
            name : 'Legacy Of Eghypt',
            path : './public/cases_img/LegacyOfEghypt.png'
        },
        case_29 : {
            name : 'Lost Relics',
            path : './public/cases_img/LostRelics.png'
        },
        case_30 : {
            name : 'Madame Destiny',
            path : './public/cases_img/MadameDestiny.png'
        },
        case_31 : {
            name : 'Magic Mirror',
            path : './public/cases_img/MagicMirror.png'
        },
        case_32 : {
            name : 'Magic Stone',
            path : './public/cases_img/MagicStone.png'
        },
        case_33 : {
            name : 'Millionaire Slot',
            path : './public/cases_img/MillionaireSlot.png'
        },
        case_34 : {
            name : 'moon',
            path : './public/cases_img/moon.png'
        },
        case_35 : {
            name : 'Mustang Gold',
            path : './public/cases_img/MustangGold.png'
        },
        case_36 : {
            name : 'Peking Luck',
            path : './public/cases_img/PekingLuck.png'
        },
        case_37 : {
            name : 'Pink Elephants',
            path : './public/cases_img/PinkElephants.png'
        },
        case_38 : {
            name : 'Pirate Gold',
            path : './public/cases_img/PirateGold.png'
        },
        case_39 : {
            name : 'Raging Rex',
            path : './public/cases_img/RagingRex.png'
        },
        case_40 : {
            name : 'Ramses Book',
            path : './public/cases_img/RamsesBook.png'
        },
        case_41 : {
            name : 'Rise Of Dead',
            path : './public/cases_img/RiseOfDead.png'
        },
        case_42 : {
            name : 'Rise of Merlin',
            path : './public/cases_img/RiseofMerlin.png'
        },
        case_43 : {
            name : 'Rise Of Olympius',
            path : './public/cases_img/RiseOfOlympius.png'
        },
        case_44 : {
            name : 'Starburst',
            path : './public/cases_img/Starburst.png'
        },
        case_45 : {
            name : 'Street',
            path : './public/cases_img/Street.png'
        },
        case_46 : {
            name : 'Sweet Bonanza',
            path : './public/cases_img/SweetBonanza.png'
        },
        case_47 : {
            name : 'The Dog House',
            path : './public/cases_img/TheDogHouse.png'
        },
        case_48 : {
            name : 'The Falcon Huntress',
            path : './public/cases_img/TheFalconHuntress.png'
        },
        case_49 : {
            name : 'The Sword & the Grail',
            path : './public/cases_img/TheSword&theGrail.png'
        },
        case_50 : {
            name : 'ThunderTruck',
            path : './public/cases_img/ThunderTruck.png'
        },
        case_51 : {
            name : 'Valley of the gods',
            path : './public/cases_img/Valleyofthegods.png'
        },
        case_52 : {
            name : 'Vikings',
            path : './public/cases_img/Vikings.png'
        },
        case_53 : {
            name : 'White Rabbit',
            path : './public/cases_img/WhiteRabbit.png'
        },
        case_54 : {
            name : 'Wolf Gold',
            path : './public/cases_img/WolfGold.png'
        },
        case_55 : {
            name : 'Yak Yeti and Roll',
            path : './public/cases_img/YakYetiandRoll.png'
        }
    };
    for (let i = 1 ; i <= 40 ; i ++) {
        let min = Math.ceil(1);
        let max = Math.floor(55);
        let rdm = Math.floor(Math.random() * (max - min +1)) + min;
        if (i == 1) {
            casiinopoly.gameboard.case1_name = 'Start';
            casiinopoly.gameboard.case1_img = './public/cases_img/case_start.png';
            casiinopoly.gameboard.case1_pion = './public/img/pion12.png';
        } else if (i == 11 || i == 31) {
            casiinopoly.gameboard[`case${i}_name`] = 'Prison';
            casiinopoly.gameboard[`case${i}_img`] = './public/cases_img/case_prison.png';
        } else if (i == 5 || i == 12 || i == 15 || i == 20 || i == 27 || i == 32 || i == 35 || i == 38) {
            casiinopoly.gameboard[`case${i}_name`] = 'Chance';
            casiinopoly.gameboard[`case${i}_img`] = './public/cases_img/case_chance.png';
        } else {
            casiinopoly.gameboard[`case${i}_name`] = casesList[`case_${rdm}`].name;
            casiinopoly.gameboard[`case${i}_img`] = casesList[`case_${rdm}`].path;
        }
    }
    let min = Math.ceil(1);
    let max = Math.floor(2);
    let rdm = Math.floor(Math.random() * (max - min +1)) + min;
    if (rdm == 1) {
        casiinopoly.turn = "j1";
        casiinopoly.infoBar.img = './public/img/pion1.png';
        casiinopoly.infoBar.msg = `C'est à ${casiinopoly.player1.nickname} de jouer`;
    } else {
        casiinopoly.turn = "j2";
        casiinopoly.infoBar.img = './public/img/pion2.png';
        casiinopoly.infoBar.msg = `C'est à ${casiinopoly.player2.nickname} de jouer`;
    }
    res.render('csnpl_game', casiinopoly);
})

app.post('/csnpl_dees', (req, res) => {
    console.log(casiinopoly);
    let turn;
    let playerNn;
    let currentPlayerCase;
    let dees = req.body.dees;
    console.log(dees);
    let playerNnNext;
    if (casiinopoly.turn == "j1") {
        turn = 1;
        playerNn = casiinopoly[`player${turn}`].nickname;
        currentPlayerCase = casiinopoly[`player${turn}`].case;
        playerNnNext = casiinopoly.player2.nickname;
    } else {
        turn = 2;
        playerNn = casiinopoly[`player${turn}`].nickname;
        currentPlayerCase = casiinopoly[`player${turn}`].case;
        playerNnNext = casiinopoly.player1.nickname;
    }
    for (let a = 0; a < dees; a++) {
        casiinopoly[`player${turn}`].case++;
    }
    console.log(casiinopoly[`player${turn}`].case);
    for (let i = 1; i < 40; i++) {
        casiinopoly.gameboard[`case${i}_pion`] = '';
    }
    if (casiinopoly[`player${turn}`].case > 40) {
        for (let i = 0; i < 40; i++) {
            casiinopoly[`player${turn}`].case--;
        }
    }
    if (casiinopoly.player1.case == casiinopoly.player2.case) {
        casiinopoly.gameboard[`case${casiinopoly.player1.case}_pion`] = './public/img/pion12.png';
    } else {
        casiinopoly.gameboard[`case${casiinopoly.player1.case}_pion`] = './public/img/pion1.png';
        casiinopoly.gameboard[`case${casiinopoly.player2.case}_pion`] = './public/img/pion2.png';
    }
    if (casiinopoly[`player${turn}`].case == 1) {
        casiinopoly.infoBar.msg = `${playerNn} est à la case départ, à ${playerNnNext} de jouer`;
        if (casiinopoly.turn == "j1") {
            casiinopoly.turn = "j2";
        } else {
            casiinopoly.turn = "j1";
        }
    } else if (casiinopoly[`player${turn}`].case == 11 || casiinopoly[`player${turn}`].case == 31) {
        casiinopoly.infoBar.msg = `${playerNn} est en prison! A ${playerNnNext} de jouer`;
        if (casiinopoly.turn == "j1") {
            casiinopoly.turn = "j2";
        } else {
            casiinopoly.turn = "j1";
        }
    } else if (casiinopoly[`player${turn}`].case == 5 || casiinopoly[`player${turn}`].case == 12 || casiinopoly[`player${turn}`].case == 15 || casiinopoly[`player${turn}`].case == 20 || casiinopoly[`player${turn}`].case == 27 || casiinopoly[`player${turn}`].case == 32 || casiinopoly[`player${turn}`].case == 35 || casiinopoly[`player${turn}`].case == 38) {
        casiinopoly.infoBar.msg = `Chance ! ${playerNn} doit piocher une carte`;
    } else {
        casiinopoly.infoBar.msg = `${playerNn} doit jouer à ${casiinopoly.gameboard[`case${casiinopoly[`player${turn}`].case}_name`]}`;
    }
    res.render('csnpl_game', casiinopoly);
});

app.post('/csnpl_chance', (req, res) => {
    const pickedCard = req.body.card;
    let currentPlayer = casiinopoly.turn;

    if (currentPlayer == "j1") {
        currentPlayer = 1;
        casiinopoly.turn = "j2";
        casiinopoly.infoBar.msg = `A ${casiinopoly.player2.nickname} de jouer`;
    } else {
        currentPlayer = 2;
        casiinopoly.turn = "j1";
        casiinopoly.infoBar.msg = `A ${casiinopoly.player1.nickname} de jouer`;
    }

    if (pickedCard == 'Air') {
        casiinopoly[`player${currentPlayer}`].higherLoss = 0;
    } else if (pickedCard == 'Terre') {
        casiinopoly[`player${currentPlayer}`].higherGain = 0;
    } else if (pickedCard == 'Tornade') {
        if (currentPlayer == 1) {
            for (let cptS = 0; cptS < casiinopoly.player1.higherGain; cptS++) {
                casiinopoly.player2.score++;
            }
        }else{
            for (let cptS = 0; cptS < casiinopoly.player2.higherGain; cptS++) {
                casiinopoly.player1.score++;
            }
        }
    } else if (pickedCard == 'Paradis') {
        if (currentPlayer == 1) {
            for (let cptS = 0; cptS > casiinopoly.player1.higherLoss; cptS--) {
                casiinopoly.player2.score--;
            }
        }else{
            for (let cptS = 0; cptS > casiinopoly.player2.higherLoss; cptS--) {
                casiinopoly.player1.score--;
            }
        }
    } else if (pickedCard == 'Gain') {
        if (currentPlayer == 1) {
            casiinopoly.player1.score = casiinopoly.player1.score + casiinopoly.player2.score;
        }else{
            casiinopoly.player2.score = casiinopoly.player2.score + casiinopoly.player1.score;
        }
    } else if (pickedCard == 'Perte') {
        if (currentPlayer == 1) {
            for (let cptS = 0; cptS < casiinopoly.player2.higherGain; cptS++) {
                casiinopoly.player1.score--;
            }
        }else{
            for (let cptS = 0; cptS < casiinopoly.player1.higherGain; cptS++) {
                casiinopoly.player2.score--;
            }
        }
    } else if (pickedCard == 'Enfer') {
        casiinopoly.player1.higherGain = 0;
        casiinopoly.player2.higherGain = 0;
    } else if (pickedCard == 'Cimetière') {
        let loss = casiinopoly.player1.higherLoss + casiinopoly.player2.higherLoss;
        if (currentPlayer == 1) {
            for (let cptS = 0; cptS > loss; cptS--) {
                casiinopoly.player1.score--;
            }
        }else{
            for (let cptS = 0; cptS > loss; cptS--) {
                casiinopoly.player2.score--;
            }
        }
    } else if (pickedCard == 'Double') {
        if (currentPlayer == 1) {
            for (let cptS = 0; cptS < casiinopoly.player1.higherGain; cptS++) {
                casiinopoly.player1.higherGain++;
            }
        }else{
            for (let cptS = 0; cptS < casiinopoly.player2.higherGain; cptS++) {
                casiinopoly.player2.higherGain++;
            }
        }
    } else if (pickedCard == 'Moitier') {
        if (currentPlayer == 1) {
            casiinopoly.player1.higherGain = casiinopoly.player1.higherGain / 2;
        }else{
            casiinopoly.player2.higherGain = casiinopoly.player2.higherGain / 2;
        }
    } else {
        return;
    };
    console.log(casiinopoly);
    res.render('csnpl_game', casiinopoly);
});

app.get('/csnpl_change', (req, res) => {
    let currentPlayer = casiinopoly.turn;
    if (currentPlayer == "j1") {
        currentPlayer = 1;
    } else {
        currentPlayer = 2;
    }
    let caseToChange = casiinopoly[`player${currentPlayer}`].case;
    let casesList = {
        case_1 : {
            name : '300 Shields',
            path : './public/cases_img/300Shields.png'
        },
        case_2 : {
            name : 'Bonanza',
            path : './public/cases_img/Bonanza.png'
        },
        case_3 : {
            name : 'Book Of Aztec',
            path : './public/cases_img/BookOfAztec.png'
        },
        case_4 : {
            name : 'Book of crazy chicken',
            path : './public/cases_img/Bookofcrazychicken.png'
        },
        case_5 : {
            name : 'Book of Dead',
            path : './public/cases_img/BookofDead.png'
        },
        case_6 : {
            name : 'Book of Lords',
            path : './public/cases_img/BookofLords.png'
        },
        case_7 : {
            name : 'Book of queen',
            path : './public/cases_img/Bookofqueen.png'
        },
        case_8 : {
            name : 'Book&bulls',
            path : './public/cases_img/Book&bulls.png'
        },
        case_9 : {
            name : 'Carnival Queen',
            path : './public/cases_img/CarnivalQueen.png'
        },
        case_10 : {
            name : 'Cazino Cosmos',
            path : './public/cases_img/CazinoCosmos.png'
        },
        case_11 : {
            name : 'Danger High Voltage',
            path : './public/cases_img/DangerHighVoltage.png'
        },
        case_12 : {
            name : 'Dead or Alive',
            path : './public/cases_img/DeadorAlive.png'
        },
        case_13 : {
            name : 'DragonHorn',
            path : './public/cases_img/DragonHorn.png'
        },
        case_14 : {
            name : 'Extra Chilli',
            path : './public/cases_img/ExtraChilli.png'
        },
        case_15 : {
            name : 'Extra Juicy',
            path : './public/cases_img/ExtraJuicy.png'
        },
        case_16 : {
            name : 'Eyes of horus',
            path : './public/cases_img/Eyesofhorus.png'
        },
        case_17 : {
            name : 'Firefly Frenzy',
            path : './public/cases_img/FireflyFrenzy.png'
        },
        case_18 : {
            name : 'Fish Golden Tank',
            path : './public/cases_img/FishGoldenTank.png'
        },
        case_19 : {
            name : 'Fish Party',
            path : './public/cases_img/FishParty.png'
        },
        case_20 : {
            name : 'Fishin Freenzy',
            path : './public/cases_img/FishinFreenzy.png'
        },
        case_21 : {
            name : 'Flame Busters',
            path : './public/cases_img/FlameBusters.png'
        },
        case_22 : {
            name : 'Jack And the Beanstalk',
            path : './public/cases_img/JackAndtheBeanstalk.png'
        },
        case_23 : {
            name : 'John Hunter and the Aztec Treasure',
            path : './public/cases_img/JohnHunterandtheAztecTreasure.png'
        },
        case_24 : {
            name : 'John hunter and the Secrets Of Da Vincis Treasure',
            path : './public/cases_img/JohnhunterandtheSecretsOfDaVincisTreasure.png'
        },
        case_25 : {
            name : 'Jungle Spirit',
            path : './public/cases_img/JungleSpirit.png'
        },
        case_26 : {
            name : 'King of the jungle',
            path : './public/cases_img/Kingofthejungle.png'
        },
        case_27 : {
            name : 'Knights life',
            path : './public/cases_img/Knightslife.png'
        },
        case_28 : {
            name : 'Legacy Of Eghypt',
            path : './public/cases_img/LegacyOfEghypt.png'
        },
        case_29 : {
            name : 'Lost Relics',
            path : './public/cases_img/LostRelics.png'
        },
        case_30 : {
            name : 'Madame Destiny',
            path : './public/cases_img/MadameDestiny.png'
        },
        case_31 : {
            name : 'Magic Mirror',
            path : './public/cases_img/MagicMirror.png'
        },
        case_32 : {
            name : 'Magic Stone',
            path : './public/cases_img/MagicStone.png'
        },
        case_33 : {
            name : 'Millionaire Slot',
            path : './public/cases_img/MillionaireSlot.png'
        },
        case_34 : {
            name : 'moon',
            path : './public/cases_img/moon.png'
        },
        case_35 : {
            name : 'Mustang Gold',
            path : './public/cases_img/MustangGold.png'
        },
        case_36 : {
            name : 'Peking Luck',
            path : './public/cases_img/PekingLuck.png'
        },
        case_37 : {
            name : 'Pink Elephants',
            path : './public/cases_img/PinkElephants.png'
        },
        case_38 : {
            name : 'Pirate Gold',
            path : './public/cases_img/PirateGold.png'
        },
        case_39 : {
            name : 'Raging Rex',
            path : './public/cases_img/RagingRex.png'
        },
        case_40 : {
            name : 'Ramses Book',
            path : './public/cases_img/RamsesBook.png'
        },
        case_41 : {
            name : 'Rise Of Dead',
            path : './public/cases_img/RiseOfDead.png'
        },
        case_42 : {
            name : 'Rise of Merlin',
            path : './public/cases_img/RiseofMerlin.png'
        },
        case_43 : {
            name : 'Rise Of Olympius',
            path : './public/cases_img/RiseOfOlympius.png'
        },
        case_44 : {
            name : 'Starburst',
            path : './public/cases_img/Starburst.png'
        },
        case_45 : {
            name : 'Street',
            path : './public/cases_img/Street.png'
        },
        case_46 : {
            name : 'Sweet Bonanza',
            path : './public/cases_img/SweetBonanza.png'
        },
        case_47 : {
            name : 'The Dog House',
            path : './public/cases_img/TheDogHouse.png'
        },
        case_48 : {
            name : 'The Falcon Huntress',
            path : './public/cases_img/TheFalconHuntress.png'
        },
        case_49 : {
            name : 'The Sword & the Grail',
            path : './public/cases_img/TheSword&theGrail.png'
        },
        case_50 : {
            name : 'ThunderTruck',
            path : './public/cases_img/ThunderTruck.png'
        },
        case_51 : {
            name : 'Valley of the gods',
            path : './public/cases_img/Valleyofthegods.png'
        },
        case_52 : {
            name : 'Vikings',
            path : './public/cases_img/Vikings.png'
        },
        case_53 : {
            name : 'White Rabbit',
            path : './public/cases_img/WhiteRabbit.png'
        },
        case_54 : {
            name : 'Wolf Gold',
            path : './public/cases_img/WolfGold.png'
        },
        case_55 : {
            name : 'Yak Yeti and Roll',
            path : './public/cases_img/YakYetiandRoll.png'
        }
    };
    let min = Math.ceil(1);
    let max = Math.floor(55);
    let rdm = Math.floor(Math.random() * (max - min +1)) + min;
    casiinopoly.gameboard[`case${caseToChange}_name`] = casesList[`case_${rdm}`].name;
    casiinopoly.gameboard[`case${caseToChange}_img`] = casesList[`case_${rdm}`].path;
    casiinopoly.infoBar.msg = `${casiinopoly[`player${currentPlayer}`].nickname} doit jouer à ${casiinopoly.gameboard[`case${caseToChange}_name`]}`;
    res.render('csnpl_game', casiinopoly);
});

app.post('/csnpl_score', (req, res) => {
    let scoreToAdd = req.body.score;
    let currentPlayer = casiinopoly.turn;
    let playerGain;
    let playerLoss;
    if (currentPlayer == "j1") {
        currentPlayer = 1;
        playerGain = casiinopoly[`player${currentPlayer}`].higherGain;
        playerLoss = casiinopoly[`player${currentPlayer}`].higherLoss;
        casiinopoly.turn = "j2";
        casiinopoly.infoBar.msg = `A ${casiinopoly.player2.nickname} de lancer les dées`;
    } else {
        currentPlayer = 2;
        playerGain = casiinopoly[`player${currentPlayer}`].higherGain;
        playerLoss = casiinopoly[`player${currentPlayer}`].higherLoss;
        casiinopoly.turn = "j1";
        casiinopoly.infoBar.msg = `A ${casiinopoly.player1.nickname} de lancer les dées`;
    }

    if (scoreToAdd > 0) {
        if (scoreToAdd > playerGain) {
            casiinopoly[`player${currentPlayer}`].higherGain = scoreToAdd;
        }
        for (let cptS = 0; cptS < scoreToAdd; cptS++) {
            casiinopoly[`player${currentPlayer}`].score++;
        }
        res.render('csnpl_game', casiinopoly);
    } else if (scoreToAdd == 0) {
        res.render('csnpl_game', casiinopoly);
    } else {
        if (scoreToAdd < playerLoss) {
            casiinopoly[`player${currentPlayer}`].higherLoss = scoreToAdd;
        }
        for (let cptS = 0; cptS > scoreToAdd; cptS--) {
            casiinopoly[`player${currentPlayer}`].score--;
            
        }
        res.render('csnpl_game', casiinopoly);
    }
});

// LION DESTINY
let liondestiny;
app.get('/liondestiny', (req, res) => {
    liondestiny = {
        turn: 1,
        card_sens: "card_dos",
        choice: "",
        a_card_game_name: "",
        a_card_game_img: '',
        a_card_turn: '',
        a_card_stack: '',
        b_card_game_name: "",
        b_card_game_img: '',
        b_card_turn: '',
        b_card_stack: '',
        c_card_game_name: "",
        c_card_game_img: '',
        c_card_turn: '',
        c_card_stack: '',
        a: {game_name: "", game_img: '', turn: '', stack: ''},
        b: {game_name: "", game_img: '', turn: '', stack: ''},
        c: {game_name: "", game_img: '', turn: '', stack: ''},
        a_status: "",
        b_status: "",
        c_status: ""
    }
    let nbTurn;
    let nbStack;
    let casesList = {
        case_1 : {
            name : '300 Shields',
            path : './public/cases_img/300Shields.png'
        },
        case_2 : {
            name : 'Bonanza',
            path : './public/cases_img/Bonanza.png'
        },
        case_3 : {
            name : 'Book Of Aztec',
            path : './public/cases_img/BookOfAztec.png'
        },
        case_4 : {
            name : 'Book of crazy chicken',
            path : './public/cases_img/Bookofcrazychicken.png'
        },
        case_5 : {
            name : 'Book of Dead',
            path : './public/cases_img/BookofDead.png'
        },
        case_6 : {
            name : 'Book of Lords',
            path : './public/cases_img/BookofLords.png'
        },
        case_7 : {
            name : 'Book of queen',
            path : './public/cases_img/Bookofqueen.png'
        },
        case_8 : {
            name : 'Book&bulls',
            path : './public/cases_img/Book&bulls.png'
        },
        case_9 : {
            name : 'Carnival Queen',
            path : './public/cases_img/CarnivalQueen.png'
        },
        case_10 : {
            name : 'Cazino Cosmos',
            path : './public/cases_img/CazinoCosmos.png'
        },
        case_11 : {
            name : 'Danger High Voltage',
            path : './public/cases_img/DangerHighVoltage.png'
        },
        case_12 : {
            name : 'Dead or Alive',
            path : './public/cases_img/DeadorAlive.png'
        },
        case_13 : {
            name : 'DragonHorn',
            path : './public/cases_img/DragonHorn.png'
        },
        case_14 : {
            name : 'Extra Chilli',
            path : './public/cases_img/ExtraChilli.png'
        },
        case_15 : {
            name : 'Extra Juicy',
            path : './public/cases_img/ExtraJuicy.png'
        },
        case_16 : {
            name : 'Eyes of horus',
            path : './public/cases_img/Eyesofhorus.png'
        },
        case_17 : {
            name : 'Firefly Frenzy',
            path : './public/cases_img/FireflyFrenzy.png'
        },
        case_18 : {
            name : 'Fish Golden Tank',
            path : './public/cases_img/FishGoldenTank.png'
        },
        case_19 : {
            name : 'Fish Party',
            path : './public/cases_img/FishParty.png'
        },
        case_20 : {
            name : 'Fishin Freenzy',
            path : './public/cases_img/FishinFreenzy.png'
        },
        case_21 : {
            name : 'Flame Busters',
            path : './public/cases_img/FlameBusters.png'
        },
        case_22 : {
            name : 'Jack And the Beanstalk',
            path : './public/cases_img/JackAndtheBeanstalk.png'
        },
        case_23 : {
            name : 'John Hunter and the Aztec Treasure',
            path : './public/cases_img/JohnHunterandtheAztecTreasure.png'
        },
        case_24 : {
            name : 'John hunter and the Secrets Of Da Vincis Treasure',
            path : './public/cases_img/JohnhunterandtheSecretsOfDaVincisTreasure.png'
        },
        case_25 : {
            name : 'Jungle Spirit',
            path : './public/cases_img/JungleSpirit.png'
        },
        case_26 : {
            name : 'King of the jungle',
            path : './public/cases_img/Kingofthejungle.png'
        },
        case_27 : {
            name : 'Knights life',
            path : './public/cases_img/Knightslife.png'
        },
        case_28 : {
            name : 'Legacy Of Eghypt',
            path : './public/cases_img/LegacyOfEghypt.png'
        },
        case_29 : {
            name : 'Lost Relics',
            path : './public/cases_img/LostRelics.png'
        },
        case_30 : {
            name : 'Madame Destiny',
            path : './public/cases_img/MadameDestiny.png'
        },
        case_31 : {
            name : 'Magic Mirror',
            path : './public/cases_img/MagicMirror.png'
        },
        case_32 : {
            name : 'Magic Stone',
            path : './public/cases_img/MagicStone.png'
        },
        case_33 : {
            name : 'Millionaire Slot',
            path : './public/cases_img/MillionaireSlot.png'
        },
        case_34 : {
            name : 'moon',
            path : './public/cases_img/moon.png'
        },
        case_35 : {
            name : 'Mustang Gold',
            path : './public/cases_img/MustangGold.png'
        },
        case_36 : {
            name : 'Peking Luck',
            path : './public/cases_img/PekingLuck.png'
        },
        case_37 : {
            name : 'Pink Elephants',
            path : './public/cases_img/PinkElephants.png'
        },
        case_38 : {
            name : 'Pirate Gold',
            path : './public/cases_img/PirateGold.png'
        },
        case_39 : {
            name : 'Raging Rex',
            path : './public/cases_img/RagingRex.png'
        },
        case_40 : {
            name : 'Ramses Book',
            path : './public/cases_img/RamsesBook.png'
        },
        case_41 : {
            name : 'Rise Of Dead',
            path : './public/cases_img/RiseOfDead.png'
        },
        case_42 : {
            name : 'Rise of Merlin',
            path : './public/cases_img/RiseofMerlin.png'
        },
        case_43 : {
            name : 'Rise Of Olympius',
            path : './public/cases_img/RiseOfOlympius.png'
        },
        case_44 : {
            name : 'Starburst',
            path : './public/cases_img/Starburst.png'
        },
        case_45 : {
            name : 'Street',
            path : './public/cases_img/Street.png'
        },
        case_46 : {
            name : 'Sweet Bonanza',
            path : './public/cases_img/SweetBonanza.png'
        },
        case_47 : {
            name : 'The Dog House',
            path : './public/cases_img/TheDogHouse.png'
        },
        case_48 : {
            name : 'The Falcon Huntress',
            path : './public/cases_img/TheFalconHuntress.png'
        },
        case_49 : {
            name : 'The Sword & the Grail',
            path : './public/cases_img/TheSword&theGrail.png'
        },
        case_50 : {
            name : 'ThunderTruck',
            path : './public/cases_img/ThunderTruck.png'
        },
        case_51 : {
            name : 'Valley of the gods',
            path : './public/cases_img/Valleyofthegods.png'
        },
        case_52 : {
            name : 'Vikings',
            path : './public/cases_img/Vikings.png'
        },
        case_53 : {
            name : 'White Rabbit',
            path : './public/cases_img/WhiteRabbit.png'
        },
        case_54 : {
            name : 'Wolf Gold',
            path : './public/cases_img/WolfGold.png'
        },
        case_55 : {
            name : 'Yak Yeti and Roll',
            path : './public/cases_img/YakYetiandRoll.png'
        }
    };
    if (liondestiny.turn > 20) {
        nbTurn = [5, 5, 5, 5, 5, 10, 10, 10, 10, 10, 15, 15, 20, 20];
        nbStack = [1, 1, 1, 2.5, 2.5, 2.5, 5];
        for (let i = 0; i < 3; i++) {
            let minT = Math.ceil(0);
            let maxT = Math.floor(13);
            let rdmT = Math.floor(Math.random() * (maxT - minT +1)) + minT;
            let minS = Math.ceil(0);
            let maxS = Math.floor(6);
            let rdmS = Math.floor(Math.random() * (maxS - minS +1)) + minS;
            let minG = Math.ceil(1);
            let maxG = Math.floor(55);
            let rdmG = Math.floor(Math.random() * (maxG - minG +1)) + minG;
            if (i == 1) {
                liondestiny.a.turn = nbTurn[rdmT];
                liondestiny.a.stack = nbStack[rdmS];
                liondestiny.a.game_name = casesList[`case_${rdmG}`].name;
                liondestiny.a.game_img = casesList[`case_${rdmG}`].path;
            } else if (i == 2) {
                liondestiny.b.turn = nbTurn[rdmT];
                liondestiny.b.stack = nbStack[rdmS];
                liondestiny.b.game_name = casesList[`case_${rdmG}`].name;
                liondestiny.b.game_img = casesList[`case_${rdmG}`].path;
            } else {
                liondestiny.c.turn = nbTurn[rdmT];
                liondestiny.c.stack = nbStack[rdmS];
                liondestiny.c.game_name = casesList[`case_${rdmG}`].name;
                liondestiny.c.game_img = casesList[`case_${rdmG}`].path;
            }
        };
    } else if (liondestiny.turn > 10) {
        nbTurn = [5, 5, 5, 5, 5, 10, 10, 10, 10, 10, 15, 15];
        nbStack = [1, 1, 1, 2.5, 2.5, 2.5];
        for (let i = 0; i < 3; i++) {
            let minT = Math.ceil(0);
            let maxT = Math.floor(11);
            let rdmT = Math.floor(Math.random() * (maxT - minT +1)) + minT;
            let minS = Math.ceil(0);
            let maxS = Math.floor(5);
            let rdmS = Math.floor(Math.random() * (maxS - minS +1)) + minS;
            let minG = Math.ceil(1);
            let maxG = Math.floor(55);
            let rdmG = Math.floor(Math.random() * (maxG - minG +1)) + minG;
            if (i == 1) {
                liondestiny.a.turn = nbTurn[rdmT];
                liondestiny.a.stack = nbStack[rdmS];
                liondestiny.a.game_name = casesList[`case_${rdmG}`].name;
                liondestiny.a.game_img = casesList[`case_${rdmG}`].path;
            } else if (i == 2) {
                liondestiny.b.turn = nbTurn[rdmT];
                liondestiny.b.stack = nbStack[rdmS];
                liondestiny.b.game_name = casesList[`case_${rdmG}`].name;
                liondestiny.b.game_img = casesList[`case_${rdmG}`].path;
            } else {
                liondestiny.c.turn = nbTurn[rdmT];
                liondestiny.c.stack = nbStack[rdmS];
                liondestiny.c.game_name = casesList[`case_${rdmG}`].name;
                liondestiny.c.game_img = casesList[`case_${rdmG}`].path;
            }
        };
    } else {
        nbTurn = [5, 5, 5, 5, 5, 10, 10, 10, 10, 10, 15];
        nbStack = [1, 1, 1, 2.5, 2.5];
        for (let i = 0; i < 3; i++) {
            let minT = Math.ceil(0);
            let maxT = Math.floor(10);
            let rdmT = Math.floor(Math.random() * (maxT - minT +1)) + minT;
            let minS = Math.ceil(0);
            let maxS = Math.floor(4);
            let rdmS = Math.floor(Math.random() * (maxS - minS +1)) + minS;
            let minG = Math.ceil(1);
            let maxG = Math.floor(55);
            let rdmG = Math.floor(Math.random() * (maxG - minG +1)) + minG;
            if (i == 1) {
                liondestiny.a.turn = nbTurn[rdmT];
                liondestiny.a.stack = nbStack[rdmS];
                liondestiny.a.game_name = casesList[`case_${rdmG}`].name;
                liondestiny.a.game_img = casesList[`case_${rdmG}`].path;
            } else if (i == 2) {
                liondestiny.b.turn = nbTurn[rdmT];
                liondestiny.b.stack = nbStack[rdmS];
                liondestiny.b.game_name = casesList[`case_${rdmG}`].name;
                liondestiny.b.game_img = casesList[`case_${rdmG}`].path;
            } else {
                liondestiny.c.turn = nbTurn[rdmT];
                liondestiny.c.stack = nbStack[rdmS];
                liondestiny.c.game_name = casesList[`case_${rdmG}`].name;
                liondestiny.c.game_img = casesList[`case_${rdmG}`].path;
            }
        };
    }
    console.log(liondestiny);
    res.render('ldtn_game', liondestiny);
});

app.get('/column_a', (req, res) => {
    liondestiny.a_card_game_name = liondestiny.a.game_name;
    liondestiny.a_card_game_img = liondestiny.a.game_img;
    liondestiny.a_card_turn = liondestiny.a.turn;
    liondestiny.a_card_stack = liondestiny.a.stack;

    liondestiny.b_card_game_name = liondestiny.b.game_name;
    liondestiny.b_card_game_img = liondestiny.b.game_img;
    liondestiny.b_card_turn = liondestiny.b.turn;
    liondestiny.b_card_stack = liondestiny.b.stack;

    liondestiny.c_card_game_name = liondestiny.c.game_name;
    liondestiny.c_card_game_img = liondestiny.c.game_img;
    liondestiny.c_card_turn = liondestiny.c.turn;
    liondestiny.c_card_stack = liondestiny.c.stack;

    liondestiny.card_sens = "card_face";
    liondestiny.a_status = "status_choice";
    liondestiny.b_status = "status_other";
    liondestiny.c_status = "status_other";
    liondestiny.choice = "a";

    res.render('ldtn_game', liondestiny);
})

app.get('/column_b', (req, res) => {
    liondestiny.a_card_game_name = liondestiny.a.game_name;
    liondestiny.a_card_game_img = liondestiny.a.game_img;
    liondestiny.a_card_turn = liondestiny.a.turn;
    liondestiny.a_card_stack = liondestiny.a.stack;

    liondestiny.b_card_game_name = liondestiny.b.game_name;
    liondestiny.b_card_game_img = liondestiny.b.game_img;
    liondestiny.b_card_turn = liondestiny.b.turn;
    liondestiny.b_card_stack = liondestiny.b.stack;

    liondestiny.c_card_game_name = liondestiny.c.game_name;
    liondestiny.c_card_game_img = liondestiny.c.game_img;
    liondestiny.c_card_turn = liondestiny.c.turn;
    liondestiny.c_card_stack = liondestiny.c.stack;

    liondestiny.card_sens = "card_face";
    liondestiny.a_status = "status_other";
    liondestiny.b_status = "status_choice";
    liondestiny.c_status = "status_other";
    liondestiny.choice = "b";

    res.render('ldtn_game', liondestiny);
})

app.get('/column_c', (req, res) => {
    liondestiny.a_card_game_name = liondestiny.a.game_name;
    liondestiny.a_card_game_img = liondestiny.a.game_img;
    liondestiny.a_card_turn = liondestiny.a.turn;
    liondestiny.a_card_stack = liondestiny.a.stack;

    liondestiny.b_card_game_name = liondestiny.b.game_name;
    liondestiny.b_card_game_img = liondestiny.b.game_img;
    liondestiny.b_card_turn = liondestiny.b.turn;
    liondestiny.b_card_stack = liondestiny.b.stack;

    liondestiny.c_card_game_name = liondestiny.c.game_name;
    liondestiny.c_card_game_img = liondestiny.c.game_img;
    liondestiny.c_card_turn = liondestiny.c.turn;
    liondestiny.c_card_stack = liondestiny.c.stack;

    liondestiny.card_sens = "card_face";
    liondestiny.a_status = "status_other";
    liondestiny.b_status = "status_other";
    liondestiny.c_status = "status_choice";
    liondestiny.choice = "c";

    res.render('ldtn_game', liondestiny);
})

app.get('/change_game', (req, res) => {
    let casesList = {
        case_1 : {
            name : '300 Shields',
            path : './public/cases_img/300Shields.png'
        },
        case_2 : {
            name : 'Bonanza',
            path : './public/cases_img/Bonanza.png'
        },
        case_3 : {
            name : 'Book Of Aztec',
            path : './public/cases_img/BookOfAztec.png'
        },
        case_4 : {
            name : 'Book of crazy chicken',
            path : './public/cases_img/Bookofcrazychicken.png'
        },
        case_5 : {
            name : 'Book of Dead',
            path : './public/cases_img/BookofDead.png'
        },
        case_6 : {
            name : 'Book of Lords',
            path : './public/cases_img/BookofLords.png'
        },
        case_7 : {
            name : 'Book of queen',
            path : './public/cases_img/Bookofqueen.png'
        },
        case_8 : {
            name : 'Book&bulls',
            path : './public/cases_img/Book&bulls.png'
        },
        case_9 : {
            name : 'Carnival Queen',
            path : './public/cases_img/CarnivalQueen.png'
        },
        case_10 : {
            name : 'Cazino Cosmos',
            path : './public/cases_img/CazinoCosmos.png'
        },
        case_11 : {
            name : 'Danger High Voltage',
            path : './public/cases_img/DangerHighVoltage.png'
        },
        case_12 : {
            name : 'Dead or Alive',
            path : './public/cases_img/DeadorAlive.png'
        },
        case_13 : {
            name : 'DragonHorn',
            path : './public/cases_img/DragonHorn.png'
        },
        case_14 : {
            name : 'Extra Chilli',
            path : './public/cases_img/ExtraChilli.png'
        },
        case_15 : {
            name : 'Extra Juicy',
            path : './public/cases_img/ExtraJuicy.png'
        },
        case_16 : {
            name : 'Eyes of horus',
            path : './public/cases_img/Eyesofhorus.png'
        },
        case_17 : {
            name : 'Firefly Frenzy',
            path : './public/cases_img/FireflyFrenzy.png'
        },
        case_18 : {
            name : 'Fish Golden Tank',
            path : './public/cases_img/FishGoldenTank.png'
        },
        case_19 : {
            name : 'Fish Party',
            path : './public/cases_img/FishParty.png'
        },
        case_20 : {
            name : 'Fishin Freenzy',
            path : './public/cases_img/FishinFreenzy.png'
        },
        case_21 : {
            name : 'Flame Busters',
            path : './public/cases_img/FlameBusters.png'
        },
        case_22 : {
            name : 'Jack And the Beanstalk',
            path : './public/cases_img/JackAndtheBeanstalk.png'
        },
        case_23 : {
            name : 'John Hunter and the Aztec Treasure',
            path : './public/cases_img/JohnHunterandtheAztecTreasure.png'
        },
        case_24 : {
            name : 'John hunter and the Secrets Of Da Vincis Treasure',
            path : './public/cases_img/JohnhunterandtheSecretsOfDaVincisTreasure.png'
        },
        case_25 : {
            name : 'Jungle Spirit',
            path : './public/cases_img/JungleSpirit.png'
        },
        case_26 : {
            name : 'King of the jungle',
            path : './public/cases_img/Kingofthejungle.png'
        },
        case_27 : {
            name : 'Knights life',
            path : './public/cases_img/Knightslife.png'
        },
        case_28 : {
            name : 'Legacy Of Eghypt',
            path : './public/cases_img/LegacyOfEghypt.png'
        },
        case_29 : {
            name : 'Lost Relics',
            path : './public/cases_img/LostRelics.png'
        },
        case_30 : {
            name : 'Madame Destiny',
            path : './public/cases_img/MadameDestiny.png'
        },
        case_31 : {
            name : 'Magic Mirror',
            path : './public/cases_img/MagicMirror.png'
        },
        case_32 : {
            name : 'Magic Stone',
            path : './public/cases_img/MagicStone.png'
        },
        case_33 : {
            name : 'Millionaire Slot',
            path : './public/cases_img/MillionaireSlot.png'
        },
        case_34 : {
            name : 'moon',
            path : './public/cases_img/moon.png'
        },
        case_35 : {
            name : 'Mustang Gold',
            path : './public/cases_img/MustangGold.png'
        },
        case_36 : {
            name : 'Peking Luck',
            path : './public/cases_img/PekingLuck.png'
        },
        case_37 : {
            name : 'Pink Elephants',
            path : './public/cases_img/PinkElephants.png'
        },
        case_38 : {
            name : 'Pirate Gold',
            path : './public/cases_img/PirateGold.png'
        },
        case_39 : {
            name : 'Raging Rex',
            path : './public/cases_img/RagingRex.png'
        },
        case_40 : {
            name : 'Ramses Book',
            path : './public/cases_img/RamsesBook.png'
        },
        case_41 : {
            name : 'Rise Of Dead',
            path : './public/cases_img/RiseOfDead.png'
        },
        case_42 : {
            name : 'Rise of Merlin',
            path : './public/cases_img/RiseofMerlin.png'
        },
        case_43 : {
            name : 'Rise Of Olympius',
            path : './public/cases_img/RiseOfOlympius.png'
        },
        case_44 : {
            name : 'Starburst',
            path : './public/cases_img/Starburst.png'
        },
        case_45 : {
            name : 'Street',
            path : './public/cases_img/Street.png'
        },
        case_46 : {
            name : 'Sweet Bonanza',
            path : './public/cases_img/SweetBonanza.png'
        },
        case_47 : {
            name : 'The Dog House',
            path : './public/cases_img/TheDogHouse.png'
        },
        case_48 : {
            name : 'The Falcon Huntress',
            path : './public/cases_img/TheFalconHuntress.png'
        },
        case_49 : {
            name : 'The Sword & the Grail',
            path : './public/cases_img/TheSword&theGrail.png'
        },
        case_50 : {
            name : 'ThunderTruck',
            path : './public/cases_img/ThunderTruck.png'
        },
        case_51 : {
            name : 'Valley of the gods',
            path : './public/cases_img/Valleyofthegods.png'
        },
        case_52 : {
            name : 'Vikings',
            path : './public/cases_img/Vikings.png'
        },
        case_53 : {
            name : 'White Rabbit',
            path : './public/cases_img/WhiteRabbit.png'
        },
        case_54 : {
            name : 'Wolf Gold',
            path : './public/cases_img/WolfGold.png'
        },
        case_55 : {
            name : 'Yak Yeti and Roll',
            path : './public/cases_img/YakYetiandRoll.png'
        }
    };
    let minG = Math.ceil(1);
    let maxG = Math.floor(55);
    let rdmG = Math.floor(Math.random() * (maxG - minG +1)) + minG;
    liondestiny[`${liondestiny.choice}_card_game_name`] = casesList[`case_${rdmG}`].name;
    liondestiny[`${liondestiny.choice}_card_game_img`] = casesList[`case_${rdmG}`].path;
    res.render('ldtn_game', liondestiny);
}) 

app.get('/new_party', (req, res) => {
    let newTurn = liondestiny.turn + 1;
    liondestiny = {
        turn: newTurn,
        card_sens: "card_dos",
        choice: "",
        a_card_game_name: "",
        a_card_game_img: '',
        a_card_turn: '',
        a_card_stack: '',
        b_card_game_name: "",
        b_card_game_img: '',
        b_card_turn: '',
        b_card_stack: '',
        c_card_game_name: "",
        c_card_game_img: '',
        c_card_turn: '',
        c_card_stack: '',
        a: {game_name: "", game_img: '', turn: '', stack: ''},
        b: {game_name: "", game_img: '', turn: '', stack: ''},
        c: {game_name: "", game_img: '', turn: '', stack: ''},
        a_status: "",
        b_status: "",
        c_status: ""
    };
    let nbTurn;
    let nbStack;
    let casesList = {
        case_1 : {
            name : '300 Shields',
            path : './public/cases_img/300Shields.png'
        },
        case_2 : {
            name : 'Bonanza',
            path : './public/cases_img/Bonanza.png'
        },
        case_3 : {
            name : 'Book Of Aztec',
            path : './public/cases_img/BookOfAztec.png'
        },
        case_4 : {
            name : 'Book of crazy chicken',
            path : './public/cases_img/Bookofcrazychicken.png'
        },
        case_5 : {
            name : 'Book of Dead',
            path : './public/cases_img/BookofDead.png'
        },
        case_6 : {
            name : 'Book of Lords',
            path : './public/cases_img/BookofLords.png'
        },
        case_7 : {
            name : 'Book of queen',
            path : './public/cases_img/Bookofqueen.png'
        },
        case_8 : {
            name : 'Book&bulls',
            path : './public/cases_img/Book&bulls.png'
        },
        case_9 : {
            name : 'Carnival Queen',
            path : './public/cases_img/CarnivalQueen.png'
        },
        case_10 : {
            name : 'Cazino Cosmos',
            path : './public/cases_img/CazinoCosmos.png'
        },
        case_11 : {
            name : 'Danger High Voltage',
            path : './public/cases_img/DangerHighVoltage.png'
        },
        case_12 : {
            name : 'Dead or Alive',
            path : './public/cases_img/DeadorAlive.png'
        },
        case_13 : {
            name : 'DragonHorn',
            path : './public/cases_img/DragonHorn.png'
        },
        case_14 : {
            name : 'Extra Chilli',
            path : './public/cases_img/ExtraChilli.png'
        },
        case_15 : {
            name : 'Extra Juicy',
            path : './public/cases_img/ExtraJuicy.png'
        },
        case_16 : {
            name : 'Eyes of horus',
            path : './public/cases_img/Eyesofhorus.png'
        },
        case_17 : {
            name : 'Firefly Frenzy',
            path : './public/cases_img/FireflyFrenzy.png'
        },
        case_18 : {
            name : 'Fish Golden Tank',
            path : './public/cases_img/FishGoldenTank.png'
        },
        case_19 : {
            name : 'Fish Party',
            path : './public/cases_img/FishParty.png'
        },
        case_20 : {
            name : 'Fishin Freenzy',
            path : './public/cases_img/FishinFreenzy.png'
        },
        case_21 : {
            name : 'Flame Busters',
            path : './public/cases_img/FlameBusters.png'
        },
        case_22 : {
            name : 'Jack And the Beanstalk',
            path : './public/cases_img/JackAndtheBeanstalk.png'
        },
        case_23 : {
            name : 'John Hunter and the Aztec Treasure',
            path : './public/cases_img/JohnHunterandtheAztecTreasure.png'
        },
        case_24 : {
            name : 'John hunter and the Secrets Of Da Vincis Treasure',
            path : './public/cases_img/JohnhunterandtheSecretsOfDaVincisTreasure.png'
        },
        case_25 : {
            name : 'Jungle Spirit',
            path : './public/cases_img/JungleSpirit.png'
        },
        case_26 : {
            name : 'King of the jungle',
            path : './public/cases_img/Kingofthejungle.png'
        },
        case_27 : {
            name : 'Knights life',
            path : './public/cases_img/Knightslife.png'
        },
        case_28 : {
            name : 'Legacy Of Eghypt',
            path : './public/cases_img/LegacyOfEghypt.png'
        },
        case_29 : {
            name : 'Lost Relics',
            path : './public/cases_img/LostRelics.png'
        },
        case_30 : {
            name : 'Madame Destiny',
            path : './public/cases_img/MadameDestiny.png'
        },
        case_31 : {
            name : 'Magic Mirror',
            path : './public/cases_img/MagicMirror.png'
        },
        case_32 : {
            name : 'Magic Stone',
            path : './public/cases_img/MagicStone.png'
        },
        case_33 : {
            name : 'Millionaire Slot',
            path : './public/cases_img/MillionaireSlot.png'
        },
        case_34 : {
            name : 'moon',
            path : './public/cases_img/moon.png'
        },
        case_35 : {
            name : 'Mustang Gold',
            path : './public/cases_img/MustangGold.png'
        },
        case_36 : {
            name : 'Peking Luck',
            path : './public/cases_img/PekingLuck.png'
        },
        case_37 : {
            name : 'Pink Elephants',
            path : './public/cases_img/PinkElephants.png'
        },
        case_38 : {
            name : 'Pirate Gold',
            path : './public/cases_img/PirateGold.png'
        },
        case_39 : {
            name : 'Raging Rex',
            path : './public/cases_img/RagingRex.png'
        },
        case_40 : {
            name : 'Ramses Book',
            path : './public/cases_img/RamsesBook.png'
        },
        case_41 : {
            name : 'Rise Of Dead',
            path : './public/cases_img/RiseOfDead.png'
        },
        case_42 : {
            name : 'Rise of Merlin',
            path : './public/cases_img/RiseofMerlin.png'
        },
        case_43 : {
            name : 'Rise Of Olympius',
            path : './public/cases_img/RiseOfOlympius.png'
        },
        case_44 : {
            name : 'Starburst',
            path : './public/cases_img/Starburst.png'
        },
        case_45 : {
            name : 'Street',
            path : './public/cases_img/Street.png'
        },
        case_46 : {
            name : 'Sweet Bonanza',
            path : './public/cases_img/SweetBonanza.png'
        },
        case_47 : {
            name : 'The Dog House',
            path : './public/cases_img/TheDogHouse.png'
        },
        case_48 : {
            name : 'The Falcon Huntress',
            path : './public/cases_img/TheFalconHuntress.png'
        },
        case_49 : {
            name : 'The Sword & the Grail',
            path : './public/cases_img/TheSword&theGrail.png'
        },
        case_50 : {
            name : 'ThunderTruck',
            path : './public/cases_img/ThunderTruck.png'
        },
        case_51 : {
            name : 'Valley of the gods',
            path : './public/cases_img/Valleyofthegods.png'
        },
        case_52 : {
            name : 'Vikings',
            path : './public/cases_img/Vikings.png'
        },
        case_53 : {
            name : 'White Rabbit',
            path : './public/cases_img/WhiteRabbit.png'
        },
        case_54 : {
            name : 'Wolf Gold',
            path : './public/cases_img/WolfGold.png'
        },
        case_55 : {
            name : 'Yak Yeti and Roll',
            path : './public/cases_img/YakYetiandRoll.png'
        }
    };
    if (liondestiny.turn > 20) {
        nbTurn = [5, 5, 5, 5, 5, 10, 10, 10, 10, 10, 15, 15, 20, 20];
        nbStack = [1, 1, 1, 2.5, 2.5, 2.5, 5];
        for (let i = 0; i < 3; i++) {
            let minT = Math.ceil(0);
            let maxT = Math.floor(13);
            let rdmT = Math.floor(Math.random() * (maxT - minT +1)) + minT;
            let minS = Math.ceil(0);
            let maxS = Math.floor(6);
            let rdmS = Math.floor(Math.random() * (maxS - minS +1)) + minS;
            let minG = Math.ceil(1);
            let maxG = Math.floor(55);
            let rdmG = Math.floor(Math.random() * (maxG - minG +1)) + minG;
            if (i == 1) {
                liondestiny.a.turn = nbTurn[rdmT];
                liondestiny.a.stack = nbStack[rdmS];
                liondestiny.a.game_name = casesList[`case_${rdmG}`].name;
                liondestiny.a.game_img = casesList[`case_${rdmG}`].path;
            } else if (i == 2) {
                liondestiny.b.turn = nbTurn[rdmT];
                liondestiny.b.stack = nbStack[rdmS];
                liondestiny.b.game_name = casesList[`case_${rdmG}`].name;
                liondestiny.b.game_img = casesList[`case_${rdmG}`].path;
            } else {
                liondestiny.c.turn = nbTurn[rdmT];
                liondestiny.c.stack = nbStack[rdmS];
                liondestiny.c.game_name = casesList[`case_${rdmG}`].name;
                liondestiny.c.game_img = casesList[`case_${rdmG}`].path;
            }
        };
    } else if (liondestiny.turn > 10) {
        nbTurn = [5, 5, 5, 5, 5, 10, 10, 10, 10, 10, 15, 15];
        nbStack = [1, 1, 1, 2.5, 2.5, 2.5];
        for (let i = 0; i < 3; i++) {
            let minT = Math.ceil(0);
            let maxT = Math.floor(11);
            let rdmT = Math.floor(Math.random() * (maxT - minT +1)) + minT;
            let minS = Math.ceil(0);
            let maxS = Math.floor(5);
            let rdmS = Math.floor(Math.random() * (maxS - minS +1)) + minS;
            let minG = Math.ceil(1);
            let maxG = Math.floor(55);
            let rdmG = Math.floor(Math.random() * (maxG - minG +1)) + minG;
            if (i == 1) {
                liondestiny.a.turn = nbTurn[rdmT];
                liondestiny.a.stack = nbStack[rdmS];
                liondestiny.a.game_name = casesList[`case_${rdmG}`].name;
                liondestiny.a.game_img = casesList[`case_${rdmG}`].path;
            } else if (i == 2) {
                liondestiny.b.turn = nbTurn[rdmT];
                liondestiny.b.stack = nbStack[rdmS];
                liondestiny.b.game_name = casesList[`case_${rdmG}`].name;
                liondestiny.b.game_img = casesList[`case_${rdmG}`].path;
            } else {
                liondestiny.c.turn = nbTurn[rdmT];
                liondestiny.c.stack = nbStack[rdmS];
                liondestiny.c.game_name = casesList[`case_${rdmG}`].name;
                liondestiny.c.game_img = casesList[`case_${rdmG}`].path;
            }
        };
    } else {
        nbTurn = [5, 5, 5, 5, 5, 10, 10, 10, 10, 10, 15];
        nbStack = [1, 1, 1, 2.5, 2.5];
        for (let i = 0; i < 3; i++) {
            let minT = Math.ceil(0);
            let maxT = Math.floor(10);
            let rdmT = Math.floor(Math.random() * (maxT - minT +1)) + minT;
            let minS = Math.ceil(0);
            let maxS = Math.floor(4);
            let rdmS = Math.floor(Math.random() * (maxS - minS +1)) + minS;
            let minG = Math.ceil(1);
            let maxG = Math.floor(55);
            let rdmG = Math.floor(Math.random() * (maxG - minG +1)) + minG;
            if (i == 1) {
                liondestiny.a.turn = nbTurn[rdmT];
                liondestiny.a.stack = nbStack[rdmS];
                liondestiny.a.game_name = casesList[`case_${rdmG}`].name;
                liondestiny.a.game_img = casesList[`case_${rdmG}`].path;
            } else if (i == 2) {
                liondestiny.b.turn = nbTurn[rdmT];
                liondestiny.b.stack = nbStack[rdmS];
                liondestiny.b.game_name = casesList[`case_${rdmG}`].name;
                liondestiny.b.game_img = casesList[`case_${rdmG}`].path;
            } else {
                liondestiny.c.turn = nbTurn[rdmT];
                liondestiny.c.stack = nbStack[rdmS];
                liondestiny.c.game_name = casesList[`case_${rdmG}`].name;
                liondestiny.c.game_img = casesList[`case_${rdmG}`].path;
            }
        };
    }
    console.log(liondestiny);
    res.render('ldtn_game', liondestiny);
})

//  EN
app.get('/home', (req, res) => {
    res.render('home');
})