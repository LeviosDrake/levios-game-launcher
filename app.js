// ELECTRON
const {BrowserWindow, app} = require('electron')
const url = require('url')
const path = require('path');
require('./index.js');


// WINDOW INIT
let mainWindow = null;

// MAIN FUNCTION
function main() {
    mainWindow = new BrowserWindow();
    mainWindow.loadURL('http://localhost:80/');
    mainWindow.focus();
    mainWindow.on('close', event => {
        mainWindow = null;
    });
};

// APP ON
app.on('ready', main);